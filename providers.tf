terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.75.0"
    }
  }

  // use this google cloud storage bucket for tfstate remote storage
  backend "gcs" {
    bucket  = "terraform-state-bucket-aaa"
  }
}

# use service account impersonation to authenticate
locals {
  tf_sa = "tom-prereq-service-account@tom-prereq-project.iam.gserviceaccount.com"
}

provider "google" {
  alias = "impersonate"
  scopes = [
    "https://www.googleapis.com/auth/cloud-platform",
    "https://www.googleapis.com/auth/userinfo.email",
  ]
}

data "google_service_account_access_token" "default" {
  provider               = google.impersonate
  target_service_account = local.tf_sa
  scopes                 = ["userinfo-email", "cloud-platform"]
  lifetime               = "600s"
}


/******************************************
  Provider credential configuration
 *****************************************/
provider "google" {
  access_token = data.google_service_account_access_token.default.access_token
}
provider "google-beta" {
  access_token = data.google_service_account_access_token.default.access_token
}
