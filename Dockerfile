# syntax=docker/dockerfile:1

# this image works on Apple M1
FROM arm64v8/ubuntu:20.04

RUN apt-get -y update && apt-get -y install apt-transport-https ca-certificates gnupg curl git
RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg  add - && apt-get update -y && apt-get install google-cloud-sdk -y

RUN apt-get update && apt-get install -y \
    wget \
    unzip \
  && rm -rf /var/lib/apt/lists/*

RUN wget --quiet https://releases.hashicorp.com/terraform/1.0.3/terraform_1.0.3_linux_arm64.zip \
  && unzip terraform_1.0.3_linux_arm64.zip \
  && mv terraform /usr/bin \
  && rm terraform_1.0.3_linux_arm64.zip

ENTRYPOINT ["tail", "-f", "/dev/null"]