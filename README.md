# practice-repo

## Main task
- Setup Local Dev Environment
  - Install Docker (DONE)
  - Install Visual Studio Code (DONE)
  - Install remote-container plugin for Visual Studio Code (DONE)
  - Install terraform plugin for Visual Studio Code (Code Linting) (DONE)
  - Write Docker file that has terraform (latest version) and gcloud (latest version) installed open code in remote-container and do development via visual studio code (DONE)
  - Store terraform state file
  - Clone repository (DONE)


- GCP Objective
  - Create 3 Projects (2 service project and 1 networking host project)  (DONE)
  - Create SVPC in host project ... create 2 subnets (DONE)
  - Shared SVPC to both projects  (DONE)
  - Create Service Perimeter (VPC Service Control... add ALL APIS)  (DONE)
  - Add 1 service project to service permieter (DONE)
  - Create storage bucket in both projects and add a random text file (DONE)

## TESTS
- Demonstrate communication within service perimeter and outside of service perimeter
- Access storage file from a VM that is within VPCSC (service project inside vpc-sc and storage within vpcsc)
- Access Storage file from outside of vpc-SC --- egress (service project inside vpc-sc and storage bucket outside of vpcsc)
- Access storage file coming ingress into vpc sc --- ingress (service project outside of vpcsc and storage bucket inside vpc-sc)

## After testing
Note: demonstrate what happens.. and create appropriate access context policies to allow these communications
Note: You will have to create ingress/egress policies to allow service project that is sitting outside of permieter to access shared vpc 
Note: For testing of copying files use gsutil in a VM




TESTS

Demonstrate communication within service perimeter and outside of service perimeter
Access storage file from a VM that is within VPCSC (service project inside vpc-sc and storage within vpcsc)
Access Storage file from outside of vpc-SC --- egress (service project inside vpc-sc and storage bucket outside of vpcsc)
Access storage file coming ingress into vpc sc --- ingress (service project outside of vpcsc and storage bucket inside vpc-sc)


After testing
Note: demonstrate what happens.. and create appropriate access context policies to allow these communications
Note: You will have to create ingress/egress policies to allow service project that is sitting outside of permieter to access shared vpc
Note: For testing of copying files use gsutil in a VM
