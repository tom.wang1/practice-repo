##########################################################
// create networks for the VPC, share vpc to both service-1 and service-2 projects
resource "google_compute_network" "custom-test-1" {
  name                    = "test-network-1"
  auto_create_subnetworks = false
  project = google_project.network.project_id
  
  depends_on = [
    google_project_service.network_api,
  ]
}

// create subnets
resource "google_compute_subnetwork" "host-subnet-1" {
  name   = "host-subnet-1"
  ip_cidr_range = "10.10.10.0/24"
  network = google_compute_network.custom-test-1.id
  project = google_project.network.project_id
  region = var.region

  // need this to enable vm without public ip to connect to GCP services
  private_ip_google_access = true
}

resource "google_compute_subnetwork" "host-subnet-2" {
  name   = "host-subnet-2"
  ip_cidr_range = "10.10.20.0/24"
  network = google_compute_network.custom-test-1.id
  project = google_project.network.project_id
  region = var.region

  private_ip_google_access = true
}

// add firewall rules by default custom network does not have any firewall rules
module "firewall_rules" {
  source       = "terraform-google-modules/network/google//modules/firewall-rules"
  project_id   = google_project.network.project_id
  network_name = google_compute_network.custom-test-1.name

  rules = [{
    name                    = "allow-ssh-ingress"
    description             = null
    direction               = "INGRESS"
    priority                = null
    ranges                  = ["0.0.0.0/0"]
    source_tags             = null
    source_service_accounts = null
    target_tags             = null
    target_service_accounts = null
    allow = [{
      protocol = "tcp"
      ports    = ["22"]
    }]
    deny = []
    log_config = {
      metadata = "INCLUDE_ALL_METADATA"
    }
  }]
  depends_on = [google_project.network]
}