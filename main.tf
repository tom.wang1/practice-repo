resource "google_folder" "department1" {
  display_name = "Toms-Department"
  parent       = "organizations/42165644541"
}

resource "google_project" "service-1" {
  name       = var.service_1
  project_id = var.service_1
  folder_id  = google_folder.department1.name
  billing_account = var.billing_id
}

# using same string for project name and id
# project id needs to be globally unique
resource "google_project" "service-2" {
  name       = var.service_2
  project_id = var.service_2
  folder_id  = google_folder.department1.name
  billing_account = var.billing_id
}

resource "google_project" "network" {
  name       = var.network_project
  project_id = var.network_project
  folder_id  = google_folder.department1.name
  billing_account = var.billing_id
}


#######################################################
// enable apis for the network project
resource "google_project_service" "network_api" {
  project = google_project.network.project_id
  service = "compute.googleapis.com"

  // disables dependent service apis when destroying
  disable_dependent_services = true

}

resource "google_project_service" "service_1_api" {
  project = google_project.service-1.project_id
  service = "compute.googleapis.com"

  // disables dependent service apis when destroying
  disable_dependent_services = true

}

resource "google_project_service" "service_2_api" {
  project = google_project.service-2.project_id
  service = "compute.googleapis.com"

  // disables dependent service apis when destroying
  disable_dependent_services = true

}


########################################################

// enables shared vpc host for the network project
resource "google_compute_shared_vpc_host_project" "svpc_host" {
  project = google_project.network.project_id

  depends_on = [
    google_project_service.network_api,
    google_project_service.service_1_api,
    google_project_service.service_2_api,
  ]
}

# share the svpc to the service projects
resource "google_compute_shared_vpc_service_project" "svpc_service1" {
  host_project    = google_compute_shared_vpc_host_project.svpc_host.id
  service_project = google_project.service-1.project_id

  depends_on = [
    google_project_service.network_api,
    google_project_service.service_1_api,
    google_project_service.service_2_api,
  ]
}

resource "google_compute_shared_vpc_service_project" "svpc_service2" {
  host_project    = google_compute_shared_vpc_host_project.svpc_host.id
  service_project = google_project.service-2.project_id

  depends_on = [
    google_project_service.network_api,
    google_project_service.service_1_api,
    google_project_service.service_2_api,
  ]
}


#########################################################################
# create buckets in both service projects

resource "google_storage_bucket" "service_1_bucket" {
  name          = "service-1-bucket"
  location      = var.region
  project       = google_project.service-1.project_id

  // destroys contents of buckets among deletion
  force_destroy = true

  // this disables ACL on buckets, access will be through IAM only
  uniform_bucket_level_access = true

}

resource "google_storage_bucket" "service_2_bucket" {
  name          = "service-2-bucket"
  location      = var.region
  force_destroy = true
  project       = google_project.service-2.project_id
  uniform_bucket_level_access = true
}


###############################################################################
# upload a random file to both buckets in both projects
# only need it when doing the initial upload after service perimeter is created

resource "google_storage_bucket_object" "service_1_bucket_upload" {
  name   = "random_text"
  source = "./random_text"
  bucket = "service-1-bucket"
}

resource "google_storage_bucket_object" "service_2_bucket_upload" {
  name   = "random_text"
  source = "./random_text"
  bucket = "service-2-bucket"
}