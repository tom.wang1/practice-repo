variable "billing_id" {
  description = "Badal billing id"
  type        = string
  default     = "01DEF7-F9833E-AD765A"
  //sensitive   = true
}

variable "region" {
  description = "default region"
  type        = string
  default     = "us-central1"
}

variable "network_project" {
  description = "network project"
  type        = string
  default     = "toms-network-project-aaaaaa"
}

variable "service_1" {
  description = "service 1 project"
  type        = string
  default     = "toms-service-1-project-aaaaaa"
}

variable "service_2" {
  description = "service 2 project"
  type        = string
  default     = "toms-service-2-project-aaaaaa"
}


// for vpc service perimeter
variable "members" {
  description = "access level members"
  type        = list
  default     = ["user:tom.wang@badal.io", 
                 "serviceAccount:tom-prereq-service-account@tom-prereq-project.iam.gserviceaccount.com",
                 "serviceAccount:tom-bucket-get-service-2@toms-service-2-project-aaaaaa.iam.gserviceaccount.com",
                 "serviceAccount:tom-bucket-get-service-1@toms-service-1-project-aaaaaa.iam.gserviceaccount.com"
                ]
}

