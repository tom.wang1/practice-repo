

# # call registry modules into local module object and use their out put values
# # this requires a service account to be created in a seed project with 
# # with service account token creator permission, 
# # Clour Resource Manager api, 
# # Access Context Manager api, 



# using existing policy in Access Context Manager
module "access_level_members" {
  source  = "terraform-google-modules/vpc-service-controls/google//modules/access_level"
  policy  = "606414454159"
  name    = "terraform_members"
  members = var.members
}



// create the perimeter that guards apis, but this also works in complementry of IAM,
// which gives service access to IAM identities
resource "google_access_context_manager_service_perimeter" "regular_service_perimeter_1" {
  parent         = "accessPolicies/606414454159"
  name           = "accessPolicies/606414454159/servicePerimeters/tom_regular_perimeter_1"
  title          = "tom_regular_perimeter_1"
  perimeter_type = "PERIMETER_TYPE_REGULAR"

  status {
    resources = ["projects/${google_project.service-1.number}", "projects/${google_project.network.number}"]
    access_levels       = ["accessPolicies/606414454159/accessLevels/${module.access_level_members.name}"]

    ingress_policies {
      ingress_from {
          sources {
              // to keep it more ganular, can put just service accounts here
              resource = "projects/${google_project.service-2.number}"
          }
          identity_type = "ANY_IDENTITY"
      }

        ingress_to {
          resources = ["projects/${google_project.service-1.number}"]
          operations {
            service_name = "storage.googleapis.com"
  
            method_selectors {
              method = "*"
            }
          }
        }
    }

    egress_policies {

      egress_from {
            identity_type = "ANY_IDENTITY"
      }

      egress_to {
        // accessing the bucket in service-2 project
        resources = ["projects/${google_project.service-2.number}" ]

        operations {
          service_name = "storage.googleapis.com"

          method_selectors {
            method = "*"
          }
        }
      }
    }

    restricted_services = ["storage.googleapis.com",
                         "pubsub.googleapis.com",
                         "bigquery.googleapis.com",
                         "monitoring.googleapis.com",
                         "logging.googleapis.com",
                         "accessapproval.googleapis.com",
                         "adsdatahub.googleapis.com",
                         "aiplatform.googleapis.com",
                         "apigee.googleapis.com",
                         "apigeeconnect.googleapis.com",
                         "artifactregistry.googleapis.com",
                         "assuredworkloads.googleapis.com",
                         "automl.googleapis.com",
                         "bigquerydatatransfer.googleapis.com",
                         "bigtable.googleapis.com",
                         "binaryauthorization.googleapis.com",
                         "cloudasset.googleapis.com",
                         "cloudbuild.googleapis.com",
                         "cloudfunctions.googleapis.com",
                         "cloudkms.googleapis.com",
                         "cloudprofiler.googleapis.com",
                         "cloudresourcemanager.googleapis.com",
                         "cloudsearch.googleapis.com",
                         "cloudtrace.googleapis.com",
                         "composer.googleapis.com",
                         "compute.googleapis.com",
                         "connectgateway.googleapis.com",
                         "container.googleapis.com",
                         "containeranalysis.googleapis.com",
                         "containerregistry.googleapis.com",
                         "containerthreatdetection.googleapis.com",
                         "datacatalog.googleapis.com",
                         "dataflow.googleapis.com",
                         "datafusion.googleapis.com",
                         "dataproc.googleapis.com",
                         "dialogflow.googleapis.com",
                         "dlp.googleapis.com",
                         "dns.googleapis.com",
                         "documentai.googleapis.com",
                         "eventarc.googleapis.com",
                         "file.googleapis.com",
                         "gameservices.googleapis.com",
                         "gkeconnect.googleapis.com",
                         "gkehub.googleapis.com",
                         "healthcare.googleapis.com",
                         "iam.googleapis.com",
                         "iaptunnel.googleapis.com",
                         "language.googleapis.com",
                         "lifesciences.googleapis.com",
                         "managedidentities.googleapis.com",
                         "memcache.googleapis.com",
                         "meshca.googleapis.com",
                         "metastore.googleapis.com",
                         "ml.googleapis.com",
                         "networkconnectivity.googleapis.com",
                         "networkmanagement.googleapis.com",
                         "networksecurity.googleapis.com",
                         "networkservices.googleapis.com",
                         "notebooks.googleapis.com",
                         "opsconfigmonitoring.googleapis.com",
                         "osconfig.googleapis.com",
                         "oslogin.googleapis.com",
                         "privateca.googleapis.com",
                         "pubsublite.googleapis.com",
                         "recaptchaenterprise.googleapis.com",
                         "recommender.googleapis.com",
                         "redis.googleapis.com",
                         "run.googleapis.com",
                         "secretmanager.googleapis.com",
                         "servicecontrol.googleapis.com",
                         "servicedirectory.googleapis.com",
                         "spanner.googleapis.com",
                         "speech.googleapis.com",
                         "sqladmin.googleapis.com",
                         "storagetransfer.googleapis.com",
                         "sts.googleapis.com",
                         "texttospeech.googleapis.com",
                         "tpu.googleapis.com",
                         "trafficdirector.googleapis.com",
                         "transcoder.googleapis.com",
                         "translate.googleapis.com",
                         "videointelligence.googleapis.com",
                         "vision.googleapis.com",
                         "vpcaccess.googleapis.com" ]
  }
}
