
# create server instances 
##############################################################################
resource "google_compute_instance" "service_1_instance" {
  name         = "service-1-instance"
  machine_type = "e2-micro"
  zone         = "us-central1-a"
  project      = google_project.service-1.project_id

  allow_stopping_for_update = true

  tags = ["foo", "bar"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    subnetwork         = "host-subnet-1"
    subnetwork_project = google_project.network.project_id
  }

  metadata = {
    foo = "bar"
  }
  
  # Google recommends custom service accounts that have cloud-platform scope 
  # and permissions granted via IAM Roles.
  service_account {
    email  = google_service_account.service_1_bucket_get.email
    scopes = ["cloud-platform"]
  }
}


resource "google_compute_instance" "service_2_instance" {
  name         = "service-2-instance"
  machine_type = "e2-micro"
  zone         = "us-central1-a"
  project      = google_project.service-2.project_id

  allow_stopping_for_update = true

  tags = ["foo", "bar"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  // default network will have https and ssh allowed in firewall rules
  network_interface {
    network = "default"
  }

  metadata = {
    foo = "bar"
  }

  # Google recommends custom service accounts that have cloud-platform scope 
  # and permissions granted via IAM Roles.
  service_account {
    email  = google_service_account.service_2_bucket_get.email
    scopes = ["cloud-platform"]
  }
}


# make these service accounts for the server instances to read Cloud Storage
#############################################################################


resource "google_service_account" "service_1_bucket_get" {
  account_id   = "tom-bucket-get-service-1"
  display_name = "A service account that reads from buckets"
  project      = google_project.service-1.project_id
}

//have to add the service account as a member to grant the role
resource "google_project_iam_member" "sa_member" {
  project = google_project.service-1.project_id
  role    = "roles/storage.objectViewer"
  member  = "serviceAccount:${google_service_account.service_1_bucket_get.email}"
}

resource "google_service_account" "service_2_bucket_get" {
  account_id   = "tom-bucket-get-service-2"
  display_name = "A service account that reads from buckets"
  project      = google_project.service-2.project_id
}

resource "google_project_iam_member" "service_2_sa_member" {
  project = google_project.service-2.project_id
  role    = "roles/storage.objectViewer"
  member  = "serviceAccount:${google_service_account.service_2_bucket_get.email}"
}



# data "google_iam_policy" "bucket_read" {
#   binding {
#     role = "roles/storage.objectViewer"

#     members = [
#     ]
#   }
# }



# resource "google_service_account_iam_policy" "bucket_read_policy" {
#   service_account_id = google_service_account.service_1_bucket_get.name
#   policy_data        = data.google_iam_policy.bucket_read.policy_data
# }




########################################################################

# resource "google_project_iam_binding" "bucket_binding_svc_1" {
#   project      = google_project.service-1.project_id

#   // this allows get, list permissions which lets view and downloading
#   role         = "roles/storage.objectViewer"

#   members = [
#     "serviceAccount:${google_service_account.service_1_bucket_get.email}]",
#   ]
# }

# resource "google_project_iam_binding" "bucket_binding_svc_2" {
#   project      = google_project.service-2.project_id
#   role         = "roles/storage.objectViewer"

#   members = [
#     "serviceAccount:${google_service_account.service_2_bucket_get.email}]",
#   ]
# }